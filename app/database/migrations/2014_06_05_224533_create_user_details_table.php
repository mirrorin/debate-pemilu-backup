<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserDetailsTable extends Migration {

	public function up()
	{
		Schema::create('user_details', function(Blueprint $table) {
			$table->increments('id')->unsigned;
			$table->integer('user_id')->unsigned();
			$table->timestamp('birth_date')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->string('potprof');
			$table->string('location');
			$table->string('job');
		});
	}

	public function down()
	{
		Schema::drop('user_details');
	}
}