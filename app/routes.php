<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::group(array('before' => 'pakeLogin'), function(){
    Route::get('', [
        'as' => 'dash',
        'uses' => 'dashboardController@getIndex'
        ]);

    Route::get('bikin-perkara', [
        'as' => 'bikinPost',
        'uses' => 'PostController@getCreate'
        ]);

    Route::post('bikin-post', [
        'as' => 'postCreatePost',
        'uses' =>'PostController@postCreatePost'
        ]);

    Route::get('testing', function(){
        // $tags = "Amsterdam,Washington,Sydney,Beijing,Cairo";
        // $tagged = explode(',', $tags);

        // $title = 'Mau kah anda memilih saya?';
        // $content = 'lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem ';
        // $post = new Post;
        // $post->title = ucwords($title);
        // $post->user_id = Sentry::getUser()->id;
        // $post->content = $content;
        // $post->up = 0;
        // $post->down = 0;
        // $post->views = 0;
        // $post->save();
        // $post->retag(["Amsterdam","Washington","Sydney","Beijing","Cairo"]);

        // $table = DB::table('post_user')->where('post_id', '=', 1)->having('user_id', '=', 2)->first();
        // $tanggapan = $table->tanggapan;

        // return $tanggapan;

        // $data = DB::table('post')
        //         ->where('post.id', '=', 2)
        //         ->join('comment', 'post.id', '=','comment.post_id')
        //         ->join('post_user', 'post.id', '=', 'post_user.post_id')
        //         ->select('comment.content', 'comment.user_id', 'comment.slug', 'comment.created_at', 'post_user.color', 'post_user.tanggapan')
        //         ->get();

        // $color = DB::table('post')
        //         ->where('post.id', '=', 1)
        //         ->orderBy('comment.created_at', 'desc')
        //         // ->leftjoin('post_user', 'post.id', '=', 'post_user.post_id')
        //         ->rightjoin('comment', 'post.id', '=','comment.post_id')
        //         ->select('comment.content', 'comment.user_id', 'comment.slug', 'comment.created_at')
        //         ->get();

        // // $color = DB::table('post_user')
        // //         ->where('post_user.post_id', '=', 1)
        // //         ->where('post_user.user_id', '=', 2)
        // //         ->select('post_user.tanggapan')
        // //         ->get();

        // return $color;

        $ikut = Post::find(2)->tagNames();

        foreach($ikut as $key => $value){
            echo $value.', ';
        }
   
    
});

    Route::get('mintajson.json', function(){
        // $post = Post::all();
        $json = Conner\Tagging\Tag::where('count', '>=', 1)->get();
        foreach ($json as $key => $value) {
            $data[] = strtolower($value['name']);
        }
        return Response::json($data);
    });

    Route::get('user/logout', [
        'as' => 'getLogout',
        'uses' => 'HomeController@getLogout'
    ]);

    Route::post('mintakejelasan', [
        'as' => 'postTanggapan',
        'uses' =>'PostController@postTanggapan'
        ]);

    Route::post('komeng', [
        'as' => 'postComment',
        'uses' => 'PostController@postComment'
        ]);

    Route::get('u/{name}', function($name){
    $user = User::where('username', '=', $name)->first();
    $data['isi'] = $user;

    // return Response::json($user);
    return View::make('belakang.profile', $data);
    });

    Route::get('api', [
        'as' => 'getSemua',
        'uses' =>'HomeController@getSemua'
    ]);

    Route::post('p/postCreateSub',[
        'as' => 'postCreateSub',
        'uses' => 'PostController@postCreateSub'
        ]);

    Route::get('profile', [
        'as' => 'getProfile',
        'uses' => 'HomeController@getProfile'
    ]);

});

Route::group(['before' => 'guest'], function(){
    Route::get('', [
        'as' => 'getHome',
        'uses' => 'HomeController@getIndex']
    );

    Route::post('login', [
            'as' => 'postLogin',
            'uses' => 'HomeController@postLogin'
            ]);

    Route::post('daftar', [
            'as' => 'postRegister',
            'uses' => 'HomeController@postRegister'
            ]);

    Route::get('about', [
        'as' => 'getAbout',
        'uses' =>'HomeController@getAbout'
    ]);

    Route::post('postLoginPlease', [
        'as' => 'postLoginPlease',
        'uses' => 'HomeController@postLoginPlease'
    ]);
      
    Route::get('{slug}', function($slug){
            Session::put('attemptedUrl', URL::current());
            // $data['comment'] = $table->comment;
            $data = Post::where('slug', '=', $slug)->first();
            // $data->id
            // $data['comment'] = Post::find($data->id)->comment()->orderBy('created_at', 'desc')->get();
            $data['comment'] =  DB::table('post')
                ->where('post.slug', '=', $slug)
                ->orderBy('comment.created_at', 'desc')
                // ->leftjoin('post_user', 'post.id', '=', 'post_user.post_id')
                ->rightjoin('comment', 'post.id', '=','comment.post_id')
                ->select('comment.id', 'comment.content', 'comment.user_id', 'comment.slug', 'comment.created_at')
                ->get();

            $updateviews = Post::find($data->id);
            $updateviews->views += 1;
            $updateviews->save();
                
            $data['int'] =DB::table('post')
                ->where('post.slug', '=', $slug)
                ->orderBy('comment.created_at', 'desc')
                // ->leftjoin('post_user', 'post.id', '=', 'post_user.post_id')
                ->rightjoin('comment', 'post.id', '=','comment.post_id')
                ->select('comment.id', 'comment.content', 'comment.user_id', 'comment.slug', 'comment.created_at')
                ->count();
            $data['data'] = Post::where('slug', '=', $slug)->first();
            return View::make('depan.post', $data);
            // return Response::json($comments);
    });

    Route::get('tags/{tags}', function($slug){
        $data['post'] = Post::withAnyTag($slug)->paginate(3);
        return View::make('belakang.index', $data);
    });



  
});