<?php
	if(Sentry::check()){
		$table = DB::table('post_user')->where('post_id', '=', $data->id)->having('user_id', '=', Sentry::getUser()->id)->first();
		if(!$table){
			$tanggapan = null;
		}else{
			$tanggapan = $table->color;
		}					
	}
?>

<div class="row">
	<div class="panel panel-default widget">
		<div class="panel-heading">
			<span class="glyphicon glyphicon-comment"></span>
			<h3 class="panel-title">Recent Comments</h3>
		</div>
		<div class="panel-body">
			@if(Sentry::check())
			@if(!isset($tanggapan))
			<div class="row" id="tanggapan">
				<p class="text-center">Silahkan Pilih Tanggapan Anda Terlebih Dahulu</p>
				<div class="col-md-6">
					<button id="pro" type="button" class="btn btn-success btn-lg btn-block">Pro</button>
				</div>
				<div class="col-md-6">
					<button id="kontra" type="button" class="btn btn-danger btn-lg btn-block">Kontra</button>
				</div>
			</div>
			@endif
			@else
			<div class="row" id="tanggapan">
				<a href="#" class="btn btn-danger btn-lg btn-block">Silahkan Login Terlebih Dahulu</a>
			</div>
			@endif

		            @if(!isset($tanggapan))
		            <form id="comment-form" method="post" style="display: none;">
			        @else
		            <form id="comment-form" method="post">
			        @endif
		            	<div class="send-wrap">
			                <textarea id="comment-area" class="form-control send-message" rows="3" placeholder="Write a reply..."></textarea>
			            </div>
			            <div class="row">
			            	<div class="col-md-12">
			            		<div class="btn-panel">
					                <a href="#" class=" col-lg-3 btn   send-message-btn " role="button"><i class="fa fa-cloud-upload"></i> Add Referensi</a>
					                <button type="submit" class="col-lg-4 text-right btn   send-message-btn pull-right"><i class="fa fa-plus"></i> Send Message</a>
					            </div>
			            	</div>	            	
			           	</div>
			        </form>
		                <ul class="list-group" id="sup-comment">
		                	<?php $x=1; ?>
		                @foreach($comment as $value)
		                    <li class="list-group-item">
		                        <div class="row">
                            <?php
                              $imgusr = User::find($value->user_id);
                            ?>
		                            <div class="col-xs-2 col-md-1">
		                                <img src="{{Gravatar::src($imgusr->email)}}" class="img-responsive" alt="" /></div>
		                            <div class="col-xs-10 col-md-11">
		                                <div>
		                                    <div class="mic-info">
		                                    <?php
												$user = DB::table('post_user')->where('post_id', '=', $data->id)->having('user_id', '=', $value->user_id)->first();
		                                    ?>
		                                        By: <a href="#" style="color: {{$user->color}}">{{User::find($value->user_id)->username}}</a> on {{date("j, F, Y", strtotime($value->created_at))}}
		                                    </div>
		                                </div>
		                                <div class="comment-text">
		                                    {{$value->content}}
		                                </div>
		                                <div class="action">
		                                	<ul class="list-inline">
											  <li><a id="subcmnt{{$x}}" data-target="{{$x}}">Reply</a></li>
											  <li>Report as Spam</li>
											</ul>
		                                </div>
		                                
		                                <ul class="list-group" id="{{$x}}" style="display:none">
		                                <?php
										$sub_comment = DB::table('sub_comment')->where('comment_id', '=', $value->id)->get(); ?>
						                    

		                                	@foreach($sub_comment as $itu)
		                                	<?php
		        								$useri = DB::table('post_user')->where('post_id', '=', $data->id)->having('user_id', '=', $itu->user_id)->first();
				                              $imgusri = User::find($itu->user_id);
				                            ?>
						                    <li class="list-group-item">
						                        <div class="row">
						                            <div class="col-xs-2 col-md-1">
						                                <img src="{{Gravatar::src($imgusri->email)}}" class="img-responsive" alt="" /></div>
						                            <div class="col-xs-10 col-md-11">
						                                <div>
						                                    <div class="mic-info">
						                                        By: <a href="#" style="color: {{$useri->color}}">{{User::find($itu->user_id)->username}}</a> on {{date("j, F, Y", strtotime($itu->created_at))}}
						                                    </div>
						                                </div>
						                                <div class="comment-text">
						                                    {{$itu->content}}
						                                </div>
						                                <div class="action">
						                                	<ul class="list-inline">
															  <li>Report as Spam</li>
															</ul>
						                                </div>
						                            </div>
						                        </div>
						                    </li>
						                   		@endforeach				                    
						                </ul>
						                <form id="cocote-comment{{$x}}" method="post" style="display:none">
						                <input type="hidden" id="ndelik{{$x}}" value="{{$value->id}}">
						                @if(!isset($tanggapan))
						                <p class="text-center">Silahkan Pilih anda pro atau kontra dengan artikel ini</p>
						                @else
						                <div class="send-wrap">
								                <textarea id="comment-area{{$x}}" class="form-control send-message" rows="3" placeholder="Write a reply..."></textarea>
								            </div>
								            <div class="row">
								            	<div class="col-md-12">
								            		<div class="btn-panel">
										                <a href="#" class=" col-lg-3 btn   send-message-btn " role="button"><i class="fa fa-cloud-upload"></i> Add Referensi</a>
										                <button type="submit" class="col-lg-4 text-right btn   send-message-btn pull-right"><i class="fa fa-plus"></i> Send Message</a>
										            </div>
								            	</div>	            	
								           	</div>
								        </form>
								        @endif

		                    </li>
		                   <?php $x++; ?>
		                    @endforeach
		                </ul>
		            </div>
		        </div>
		    </div>

@section('js')
@if(Sentry::check())
<script type="text/javascript">
	 	$(function(){
 		 @for($i=1; $i <= $int; $i++)
	 	$('#cocote-comment{{$i}}').on('submit', function() 
    	{                
        	$.ajax({
                    "type": "POST",
                    "url": "{{URL::route('postCreateSub')}}",
                    "data": {
                        "commentId" : $('#ndelik{{$i}}').val(),
                        "userId" : {{Sentry::getUser()->id}},
                       	"content" : $("#comment-area{{$i}}").val()
                    },
                    "dataType": "json"
                }).done(function(data) 
                {
                	$('#{{$i}}').append(
                		'<li class="list-group-item">'+
	                        '<div class="row">'+
	                            '<div class="col-xs-2 col-md-1">'+
	                                '<img src="{{Gravatar::src(Sentry::getUser()->email)}}" class="img-responsive" alt="" /></div>'+
	                            '<div class="col-xs-10 col-md-11">'+
	                                '<div>'+
	                                    '<div class="mic-info">'+
	                                        'By: <a href="#" style="color: {{$tanggapan}}">'+data.username+'</a> on '+data.date+
	                                    '</div>'+
	                                '</div>'+
	                                '<div class="comment-text">'+
	                                    data.content+
	                                '</div>'+
	                                '<div class="action">'+
	                                	'<ul class="list-inline">'+
										  '<li>Report as Spam</li>'+
										'</ul>'+
	                                '</div>'+
	                            '</div>'+
	                        '</div>'+
	                    '</li>');
            });
            return false;
    	});
		@endfor
	 	  @for($i=1; $i <= $int; $i++)
	 		$('#subcmnt{{$i}}').click(function(){
		 		var data = $(this).data('target');
		 		$('#'+data).toggle(50);

		 		$('#cocote-comment{{$i}}').toggle(50);
		 	});
		@endfor

	 	$('#pro').click(function(event){
	 		$('#tanggapan').remove();
	 		$.ajax({
                    "type": "POST",
                    "url": "{{URL::route('postTanggapan')}}",
                    "data": {
                        "postId" : '{{$data->id}}',
                        "userId" : '{{Sentry::getUser()->id}}', 
                        "tanggapan" : 'pro'
                    },
                    "dataType": "json"
                }).done(function(result) 
                {
                	$('#comment-form').fadeIn(1000);
            });
            return false;
	 	});

	 	$('#kontra').click(function(event){
	 		$('#tanggapan').remove();
	 		$.ajax({
                    "type": "POST",
                    "url": "{{URL::route('postTanggapan')}}",
                    "data": {
                        "postId" : '{{$data->id}}',
                        "userId" : '{{Sentry::getUser()->id}}', 
                        "tanggapan" : 'kontra'
                    },
                    "dataType": "json"
                }).done(function(result) 
                {
                	$('#comment-form').fadeIn(1000);
            });
            return false;
	 	});

	 	$('#comment-form').on('submit', function() 
        {                
            $.ajax({
                "type": "POST",
                "url": "{{URL::route('postComment')}}",
                "data": {
                    "postId" : '{{$data->id}}',
                    "userId" : '{{Sentry::getUser()->id}}',
                    "content" : $("#comment-area").val()
                },
                "dataType": "json"
            }).done(function(data) 
            { 	
            	$('#sup-comment').prepend(
                	'<li class="list-group-item">'+
                        '<div class="row">'+
                            '<div class="col-xs-2 col-md-1">'+
                                '<img src="http://placehold.it/80" class="img-responsive" alt="" /></div>'+
                            '<div class="col-xs-10 col-md-11">'+
                                '<div>'+
                                    '<div class="mic-info">'+
                                         'By: <a href="#" style="color: '+data.warna+'">'+data.username+'</a> on '+data.date+
                                   '</div>'+
                                '</div>'+
                                '<div class="comment-text">'+
                                    data.content+
                                '</div>'+
                               ' <div class="action">'+
                                	'<ul class="list-inline">'+
									  '<li>Tanggapi (3)</li>'+
									  '<li>Report as Spam</li>'+
									'</ul>'+
                                '</div>'+
                                '<ul class="list-group">'+
				  
				                '</ul>'+
				                '<div class="send-wrap ">'+
					                '<textarea class="form-control send-message" rows="3" placeholder="Write a reply..."></textarea>'+
					            '</div>'+
					            '<div class="btn-panel">'+
					                '<a href="" class=" col-lg-3 btn   send-message-btn " role="button"><i class="fa fa-cloud-upload"></i> Add Referensi</a>'+
					                '<a href="" class=" col-lg-4 text-right btn   send-message-btn pull-right" role="button"><i class="fa fa-plus"></i> Send Message</a>'+
					            '</div>'+
                            '</div>'+
                       ' </div>'+
                    '</li>'
                	);
                
                $('.bottom-right').notify({
                    type: 'success',
                    fadeOut : {
                        enabled : true,
                        delay : 3000
                    },
                    message: {
                        text:'Comment Published!',
                    }
                  }).show();
            });                
            return false;
        });

				
	 });
</script>\
@endif
@stop

