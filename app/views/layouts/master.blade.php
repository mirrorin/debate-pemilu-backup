<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <title>Debat Pemilu</title>
@yield('meta')   
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="http://apipemilu-caleg.s3-website-ap-southeast-1.amazonaws.com/stamps/stamps-animated.gif">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootswatch/latest/yeti/bootstrap.min.css">
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
@yield('css')
{{ HTML::style('assets/css/bootstrap-notify.css') }}
{{ HTML::style('assets/css/main.css') }}

</head>
<body>
@include('layouts.navbar')
@yield('content')
<div class='notifications bottom-right'></div>
<div id="footer">
  <div class="container">
  	<div class="text-center">
  		<p class="text-muted">Build With <i class="fa fa-heart"></i> From Yogyakarta</p>
  	</div>    
  </div>
</div>
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
{{ HTML::script('assets/js/bootstrap-notify.js') }}
{{ HTML::script('assets/js/base.js') }}

@yield('js')
@if(!Sentry::check())
<script type="text/javascript">
     $(function(){
            $('#login-form').on('submit', function() 
            {
                var remember = false;
                if($("#remember").is(':checked'))
                {
                    remember = true;
                }
                
                $.ajax({
                    "type": "POST",
                    "url": "{{URL::route('postLogin')}}",
                    "data": {
                        "email" : $('#email').val(),
                        "pass" : $('#pass').val(), 
                        "remember" : remember
                    },
                    "dataType": "json"
                }).done(function(result) 
                { 
                    if(result.logged === false)
                    {
                        if(typeof result.errorMessage !== 'undefined')
                        {
                             $('.bottom-right').notify({
                                type: 'danger',
                                message: {
                                    text:result.errorMessage,
                                }
                              }).show();
                        }
                    } else {
                        window.location = "{{Session::get('attemptedUrl')}}";
                    }
                });
                
                return false;
            });
        });
</script>
@endif
</body>
</html>