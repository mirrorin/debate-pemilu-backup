@extends('layouts.master')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-9 post">
			<div class="page-header">
			  <h1>{{$data->title}}</h1>
			</div>
			<p>{{$data->content}}<p>
			<hr>
			<div class="row">
			    <div class="col-md-12">
			    	<div class="pull-left">
			    		<ul class="list-inline">
			    			<li><a href="{{URL::to('u/'.User::find($data->user_id)->username)}}">{{User::find($data->user_id)->username}}</a></li>
			    		</ul>
			    	</div>
			    	<div class="pull-right">
						<ul class="list-inline">
						  <li>{{$data->views}} View</li>
						  <li>{{Post::find($data->id)->comment()->count()}} Reply</li>
						  <li>Report as Spam</li>
						</ul>
					</div>
			    </div>						         
		     </div>
		     @include('includ.comment')
		     <!-- AddThis Pro BEGIN -->
				 <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-535e539e54531802"></script>
		<!-- AddThis Pro END -->
		</div>
		<div class="col-md-3">
		</div>
	</div>
</div>
@stop