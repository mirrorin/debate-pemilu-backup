@extends('layouts.master')

    @section('meta')
    <meta name="robots" content="index, follow" />
    <link rel="canonical" href="{{URL::current()}}" />
    @stop

@section('content')
<div class="container">
    <div class="row">
        @include('includ.posttumb')
        @if(!Sentry::check())
        <div class="col-md-4">
            <legend>Register Now!</legend>
            <div class="panel panel-default">
                <div class="panel-heading"> <strong class="">Register</strong>

                </div>
                <div class="panel-body">
                    <!-- <form method="post" class="form-horizontal" role="form" id="register-form"> -->
                     <form id="register-form" method="post" class="form-horizontal">
                        <div class="form-group">
                            <label for="inputEmail" class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" id="inputEmail" placeholder="Email" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputUsername" class="col-sm-3 control-label">Username</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="inputUsername" placeholder="Username" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword" class="col-sm-3 control-label">Password</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" id="inputPassword" placeholder="Password" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword2" class="col-sm-3 control-label"></label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" id="inputPassword2" placeholder="Repeat Password" required="">
                            </div>
                        </div>
                        <div class="form-group last">
                            <div class="col-sm-offset-3 col-sm-9">
                                <input type="submit" class="btn btn-success btn-sm" value="Register">
                                <button type="reset" class="btn btn-default btn-sm">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@stop

@section('js')
@parent
<script type="text/javascript">
     $(function(){

            $('#register-form').on('submit', function() 
            {
                var email = $('#inputEmail').val();
                var pass1 = $("#inputPassword").val();
                var pass2 = $("#inputPassword2").val();
                if(pass1 != pass2)
                {
                    $('.bottom-right').notify({
                        type: 'danger',
                        message: {
                            text:'Repeat Password Salah!',
                        }
                      }).show();   
                }
                
                $.ajax({
                    "type": "POST",
                    "url": "{{URL::route('postRegister')}}",
                    "data": {
                        "email" : $('#inputEmail').val(),
                        "username" : $('#inputUsername').val(), 
                        "password" : $('#inputPassword').val()
                    },
                    "dataType": "json"
                }).done(function() 
                { 
                        alert('jembut');
                });
                return false;
            });
        });
</script>
@stop